require 'uri'
require 'open-uri'
require 'nokogiri'
require 'colorize'
require 'io/console'
require 'open3'

puts
puts '    ╔═════════════════════╗'.red
puts '    ║                     ║'.red
puts '    ║         ***         ║'.red
puts '    ║   SONGDOWNLOADER    ║'.red
puts '    ║  by radioelephant   ║'.red
puts '    ║         ***         ║'.red
puts '    ║                     ║'.red
puts '    ╚═════════════════════╝'.red
puts
puts
puts 'This script takes ./songs.txt and for each line it searches YouTube and offers the first page of results for the user to choose the correct video from. After all searches have been complete, the selected videos will be downloaded and converted into mp3 format. If a search or download is unsuccessful the search string will be written to ./songs.txt, so it can be tried again later. The songs will be saved to the ./songs directory. To download and extract music from YouTube videos the linux package `youtube-dl` is needed. Please make sure youtube-dl is installed.'.green
puts
puts 'This is how it\'s gonna go down:'.green
puts '  1. I will search YouTube for your song'.green
puts '  2. You will select the correct video to donwload the song from'.green
puts '  3. I will save the url to the video for later'.green
puts '  4. Steps 1-3 will be repeated for each song'.green
puts '  5. After selecting a video for each desired song I will download them all'.green
puts

Open3.popen3('which youtube-dl') do |stdin, stdout, stderr, wait_thr|
  unless wait_thr.value.success?
    puts
    puts 'The linux package `youtube-dl` is missing.'.red
    puts 'Please install youtube-dl and try again.'.blue
    puts
    puts 'You can install youtube-dl with the package-manager of your distribution:'.green
    puts '  Fedora:         dnf install youtube-dl'.green
    puts '  Debian/Ubuntu:  apt install youtube-dl'.green
    puts '  Arch:           pacman -S youtube-dl (development version is in the AUR as youtube-dl-git)'.green
    puts
    exit 1
  end
end


songs_from_file = Array.new

File.foreach('./songs.txt') do |line|
  songs_from_file.push line.strip
end

puts "Found #{songs_from_file.count} songs to download".magenta
puts
if songs_from_file.count < 1
  puts 'Input file is empty, no songs to download.'.red
  puts
  exit 0
end
puts 'Press any key to continue...'.blue
STDIN.getch

failed = Array.new
songs = Array.new

songs_from_file.each.with_index(1) do |song, index|
  puts
  puts "================================================================================"
  puts
  puts "Song (#{index}/#{songs_from_file.count}): #{song}"
  url = "https://www.youtube.com/results?search_query=#{URI.escape(song)}"
  doc = Nokogiri::HTML(URI.open(url).read)

  linkblocks = doc.xpath("//div[@id='results']/ol[@class='section-list']/li/ol[@class='item-section']/li")

  puts
  puts 'ID | Duration | Title                                                                            | Views       | Uploaded        | YouTube ID'
  puts '---+----------+----------------------------------------------------------------------------------+-------------+-----------------+-----------'

  max_linkblocks = 0
  current_song_link_hashes = []

  linkblocks.each.with_index(1) do |lb, index|
    next if index > 25

    duration = lb.xpath(".//span[@class='video-time']").text
    views = lb.xpath(".//div[@class='yt-lockup-content']//ul[@class='yt-lockup-meta-info']//li[2]").text
    title = lb.xpath(".//h3/a").text
    uploaded = lb.xpath(".//div[@class='yt-lockup-content']//ul[@class='yt-lockup-meta-info']//li[1]").text
    link = lb.xpath(".//h3/a").attr('href')

    o_id = index
    o_id = " " + o_id.to_s if index < 10

    o_dur = duration
    o_dur = "0" + o_dur if o_dur.length < 5
    (8 - o_dur.length).times { o_dur = " " + o_dur } if o_dur.length < 8
    o_dur = o_dur[0..5] + "++" if o_dur.length > 8

    o_title = "\"#{title}\""
    (80 - o_title.length).times { o_title += "." } if o_title.length < 80
    o_title = o_title[0..77] + ">>" if o_title.length > 80

    o_views = views.strip.chomp(' views')
    (11 - o_views.length).times { o_views = " " + o_views } if o_views.length < 11
    o_views = "999,999,99+" if o_views.length > 11

    o_age = uploaded.strip
    (15 - o_age.length).times { o_age = " " + o_age } if o_age.length < 15
    o_age = o_age[0..12] + '>>'  if o_age.length > 15

    key = link.to_s.sub(/^\/watch\?v=/, '').strip

    unless o_views.strip == ""
      puts "#{o_id} | #{o_dur} | #{o_title} | #{o_views} | #{o_age} | #{key}"
      max_linkblocks = index
      # Add hash to array, to later search it according to user input and select the correct song
      current_song_link_hash = { id: index, title: title, key: key }
      current_song_link_hashes.push current_song_link_hash
    end
  end

  puts

  selection = 0
  # while !(1..max_linkblocks).include?(selection.to_i) do
  while !current_song_link_hashes.any? { |sl| sl[:id] == selection.to_i } do
    print "Please select desired video by ID (1-#{max_linkblocks}) or \"s\" to skip: ".blue
    selection = gets.chomp
    break if selection == "q" || selection == "s"
  end

  if selection == "q" || selection == "s"
    puts
    puts 'No video selected for the current song, skipping...'.yellow
    puts 'Adding song to fails and writing back to songs.txt'.yellow
    failed.push song
    next
  end

  selected_song_link_hash = current_song_link_hashes.select{ |sl| sl[:id] == selection.to_i }.first

  puts
  puts "For Song: #{song}".green
  puts "Selected ID #{selection}: #{selected_song_link_hash[:title]}".yellow

  song_url = "https://www.youtube.com/watch?v=#{selected_song_link_hash[:key]}"
  songs.push({url: song_url, title: selected_song_link_hash[:title], query: song})

  puts "YouTube URL: #{song_url} written to list of downloads".yellow
  puts
  puts "================================================================================"
  puts
  puts '...next song...' unless index == songs_from_file.count
end

# Append all the Links to the links.txt file here
f = File.new './links.txt', 'a'
songs.each do |song|
  f.write song[:url] + "\n"
end
f.close

# Read all lines from links file and merge hash with existing
songs_to_dl = []
File.foreach('./links.txt') do |line|
  songs_to_dl.push({ url: line.strip })
end

merged_songs = []
songs_to_dl.each do |std|
  matching_song = songs.select{ |s| s[:url] == std[:url] }.first
  if matching_song.nil?
    merged_songs.push std
  else
    merged_songs.push std.merge matching_song
  end
end

puts
puts '================================================================================'.green
puts '================================================================================'.green
puts '================================================================================'.green
puts
puts 'Finished selecting videos. Will now start to download and extract audio from them.'.green

Dir.mkdir 'songs' unless Dir.exist? 'songs'

merged_songs.each.with_index(1) do |song, index|
  puts
  puts "Downloading song #{index} of #{merged_songs.count} ...".green
  if song[:query].nil?
    puts 'Song was not added via songdownloader.rb, but link to song was written to links.txt.'.red
  else
    puts "Searched for: #{song[:query]}".yellow
    puts "Selected video: #{song[:title]}".yellow
  end
  puts "URL: #{song[:url]}".yellow
  puts

  puts "Download the song here... Evaluate if successful, if not push to failed"
  ytdl_retval = system "youtube-dl --abort-on-error -f bestaudio --extract-audio --audio-format mp3 --audio-quality 0 --no-overwrites --exec 'mv {} ./songs/{}' #{song[:url]}"

  puts
  if ytdl_retval
    puts 'Downloaded song successfully'.green
  else
    puts 'Downloading of song failed'.red
    if song[:query].nil?
      puts 'Link was manually added to links.txt, adding link to songs.txt for next time.'.yellow
      failed.push song[:url]
    else
      puts 'Original input aswell as selected url added to songs.txt for next time.'.yellow
      failed.push "#{song[:query]} #{song[:url]}"
    end
  end
end

# Write the search strings for all failed songs back to file, for next time
f = File.new './songs.txt', 'w' # File gets overwritten here
failed.each do |failing|
  f.write failing
end
f.close

puts

if failed.count > 0
  puts "There were #{failed.count} errors.".red
else
  puts 'There were no errors.'.green
end

puts
puts 'Finished'.blue

exit 0
